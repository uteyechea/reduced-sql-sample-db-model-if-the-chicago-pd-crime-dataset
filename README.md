# Reduced SQL sample DB model if the Chicago PD crime dataset

A simplified model for the project found [here](https://gitlab.com/uteyechea/sql-sample-dw-model-of-the-chicago-pd-crime-dataset). 
It is simplified or reduced because it only includes 4 of the 7 dimensions of the original data warehouse found [here](https://gitlab.com/uteyechea/sql-sample-dw-model-of-the-chicago-pd-crime-dataset). 
We will use the Greedy algorithm for views materialization in a data cube found [here](https://gitlab.com/uteyechea/a-greedy-algorithm-for-efficient-implementation-of-data-cubes-in-python) 
To optimize the views to materialize.
