create database dwRadar_4dimSelected
use dwRadar_4dimSelected

/* Dimension No 1: time */
declare @start_dt as date = '1/1/2015';		-- Date from which the calendar table will be created.
declare @end_dt as date = '1/1/2021';		-- Calendar table will be created up to this date (not including).

create table dim_time(
 pk_time_id int primary key identity(1,1),
 date_year smallint,
 date_month tinyint,
 date_day tinyint,
 quarter_id tinyint
)
while @start_dt < @end_dt
begin
	insert into dim_time(date_year, date_month, date_day, quarter_id)	
	values( year(@start_dt), month(@start_dt), day(@start_dt), datepart(quarter, @start_dt) )
	set @start_dt = dateadd(day, 1, @start_dt)
end

--Was the insertion successful?
select * from dim_time order by pk_time_id --SC 2192


--Dimension No.2 : location
create table dim_location(
pk_location_id int primary key identity(1,1),
block varchar(50)
)
insert into dim_location(block)
select distinct block from dbRadar.dbo.chicago_crime_data
--Was the insertion successful?
select * from dim_location order by block desc --SC 31 543
select distinct block from dbRadar.dbo.chicago_crime_data --31 543

--Dimension No. 3: crime primary type
create table dim_crime(
pk_primaryType_id int primary key identity(1,1),
primaryType varchar(50)
)
insert into dim_crime(primaryType)
select distinct Primary_Type from dbRadar.dbo.chicago_crime_data
--Was the insertion successful?
select * from dim_crime order by pk_primaryType_id desc --SC 34

create table dim_arrest(
pk_arrest_id int primary key identity(1,1),
arrest bit
)
insert into dim_arrest(arrest) values
('true'),
('false')
--Was the insertion successful?
select * from dim_arrest order by pk_arrest_id desc --SC 2

--Measure No. 1
create table measure_crimeCount(
crimeCount int
)
insert into measure_crimeCount values
(1)

--Fact table
create table fact_chicagoCrime(
fk_time_id int foreign key(fk_time_id) references dim_time(pk_time_id),
fk_location_id int foreign key(fk_time_id) references dim_location(pk_location_id),
fk_arrest_id int foreign key(fk_arrest_id) references dim_arrest(pk_arrest_id),
fk_primaryType_id int foreign key(fk_primaryType_id) references dim_crime(pk_primaryType_id),
crimeCount int,
constraint pk_chicagoCrime_id primary key clustered
(fk_time_id,fk_location_id,fk_arrest_id,fk_primaryType_id,crimeCount)
)
insert into fact_chicagoCrime
select 
pk_time_id,
pk_location_id,
pk_arrest_id,
pk_primaryType_id,
count(crimeCount)

from 
dim_time,
dim_location,
dim_arrest,
dim_crime,
measure_crimeCount,
dbRadar.dbo.chicago_crime_data as db

where 
year(db.Date) = dim_time.date_year and month(db.Date)=dim_time.date_month and day(db.Date)=dim_time.date_day and
db.Block=dim_location.block and
db.Arrest = dim_arrest.arrest and 
db.Primary_Type = dim_crime.primaryType

group by 
pk_time_id,
pk_location_id,
pk_arrest_id,
pk_primaryType_id


select * from fact_chicagoCrime order by crimeCount desc --SC 1 023 860

select top 10 * from dim_location order by block

/*
Let A be the dimension dim_location,
B be the dimension dim_time,
C be the dimension dim_crime and
D be the dimension dim_arrest
*/

--Storage Cost for Dimension ABCD = 1 023 860

--Storage Cost for Dimension ABC = 1 019 641
select fk_location_id,fk_time_id,fk_primaryType_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_location_id, fk_time_id, fk_primaryType_id
--Storage Cost for Dimension ABD = 992 667
select fk_location_id,fk_time_id, fk_arrest_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_location_id, fk_time_id, fk_arrest_id
--Storage Cost for Dimension ACD = 290 355
select fk_location_id,fk_primaryType_id, fk_arrest_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_location_id,fk_primaryType_id, fk_arrest_id
--Storage Cost for Dimension BCD = 49 909
select fk_time_id,fk_primaryType_id, fk_arrest_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_time_id, fk_primaryType_id, fk_arrest_id
--Storage Cost for Dimension AB = 975 380
select fk_location_id,fk_time_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_location_id, fk_time_id
--Storage Cost for Dimension AC = 241 820
select fk_location_id,fk_primaryType_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_location_id, fk_primaryType_id
--Storage Cost for Dimension AD = 54 765
select fk_location_id, fk_arrest_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_location_id, fk_arrest_id
--Storage Cost for Dimension BC = 31 171
select fk_time_id,fk_primaryType_id, count(crimeCount)
from fact_chicagoCrime 
group by fk_time_id, fk_primaryType_id
--Storage Cost for Dimension BD = 2 916
select fk_time_id, fk_arrest_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_time_id, fk_arrest_id
--Storage Cost for Dimension CD = 66
select fk_primaryType_id, fk_arrest_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_primaryType_id, fk_arrest_id
--Storage Cost for Dimension A = 31 543
select fk_location_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_location_id
--Storage Cost for Dimension B = 1 458
select fk_time_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_time_id
--Storage Cost for Dimension C = 34
select fk_primaryType_id, count(crimeCount)
from fact_chicagoCrime 
group by fk_primaryType_id
--Storage Cost for Dimension D = 2
select fk_arrest_id,count(crimeCount)
from fact_chicagoCrime 
group by fk_arrest_id


















--Storage Cost for Dimension AB= 



